package main

import (
	"flag"
	"fmt"
	"os"

	"github.com/keybase/go-keybase-chat-bot/kbchat"
)

func fail(msg string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, msg+"\n", args...)
	os.Exit(3)
}

func main() {
	var kbLoc string
	var kbc *kbchat.API
	var err error

	flag.StringVar(&kbLoc, "keybase", "keybase", "the location of the Keybase app")
	flag.Parse()

	if kbc, err = kbchat.Start(kbchat.RunOptions{KeybaseLocation: kbLoc}); err != nil {
		fail("Error creating API: %s", err.Error())
	}
	chats, err := kbc.GetConversations(false)
	if err != nil {
		fail("Cannot get chat list: %s", err.Error())
	}
	cID := ""
	for _, v := range chats {
		fmt.Printf("ID:%s ChanelName:%s\n", v.Id, v.Channel.Name)
		if v.Channel.Name == kbc.GetUsername() {
			cID = v.Id
			break
		}
	}
	// Send a message to myself
	rsp, err := kbc.SendMessageByConvID(cID, "Hello, I'm a bot :)")
	fmt.Printf("Response: %s\n", rsp.Result.Message)

}
