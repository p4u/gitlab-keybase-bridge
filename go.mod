module gitlab.com/akvo/gitlab-keybase-bridge

go 1.13

require (
	github.com/keybase/go-keybase-chat-bot v0.0.0-20191212013139-6edf0259a3e9 // indirect
	github.com/stretchr/testify v1.4.0 // indirect
)
